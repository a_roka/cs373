---
title: Week 13
date: 2018-12-02
---

## What did you do this past week?

This past week the final lectures were all over refactoring models on the movie rentals example. It was a good reminder to explore how refactoring makes code more flexible rather than just more efficient or pretty. Our group also spent a lot of time meeting up to complete our presentation and practice it, as well as create the D3 visualizations of our provider. 

## What's in your way?

The only things left in my way for this course is the final presentation and Exam 2. Since our group gets to present first thing on Monday morning, it will be nice to have the rest of the week not worrying about that on top of the exam. That being said, I also have a final report due on Wednesday and another final presentation on Thursday. 

## What will you do next week?

Next week will be everyone’s presentations for their IDB project. Our group, CharityLink (group nine), will actually be the very first ones to present on Monday at 10 a.m. There will be three groups in our class presenting each day, except for Friday when only two groups will go up. We are pretty happy to be the first ones to talk so we can get it over with and view everyone else’s finished product. 

## If you went, what did you think of the talk by Google?

Friday evening’s talk was one of my favorites just because it was very personal, very insightful, and there was no projected presentation that the speakers were reading off of. Everything we spoke about felt genuine and was seemingly off the cuff. 

## What was your experience of the refactoring topics?

While the movie rentals example was useful to fully explore, it’s rather difficult to keep track of all the changes you can make, especially when you are working with a new and full project in real life applications. The nomenclature of the different refactoring patterns is also pretty vague. While I’m sure the two-worded method name can be thoroughly explained to match the concept of the refactoring, I don’t find it any easier to remember what changes are going to be applied just from the name. 

## What's your pick-of-the-week or tip-of-the-week?

My tip of the week is to explore other studies outside of computer science, and if possible even outside of natural sciences. By this, I actually mean papers, books, or talks on economics, psychology, sociology, etc., not another hobby or physical activity like soccer or dancing (these are great, too, and you should keep doing what you do). This was emphasized during a portion of the Google talk on Friday and I think it’s something that should be valued and practiced more often between studies. Especially if you consider yourself to be a very analytical person, a lot of other fields of study could use outsider input to make things more understandable and gain a perspective that might not have existed before. Plus you’ll probably learn something new and eventually something very interesting. 
