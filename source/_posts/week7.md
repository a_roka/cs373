---
title: Week 7
date: 2018-10-14
---

## What did you do this past week? 

This past week we went over functions and more specific details of function calling in Python (default arguments, packing and unpacking, etc.). I also had some time to get ahead of classwork for other courses so that I could use this weekend to review for the exam and work on some papers. 

## What's in your way? 

As of right now really just the exam for this class. After that, I will be able to worry about Phase 2 of the group project. In terms of other classes, I still have to figure out an idea for a research proposal for a lab, and then finalize a separate one for my Systems Bio class. 

## What will you do next week? 

On Monday will be our first exam for this class so that will be interesting to see what exactly we will have to code for it. It will actually be the last exam of the first round for this semester for me. Next week I also have to submit a couple of research proposals which I am working on when taking a break from studying for this exam. After those are finished I will be diving into some work on Phase 2 of our project. 

## What was your experience of learning the basics of Python? 

I thought it was awesome. I learned Python when I came to Austin just by translating material I had already written in Java and C, so I have never actually sifted through a textbook and delved into the details. So far, this class has been amazing at exploring what is under the hood and making all of the nuances (from syntax to function) of Python apparent. I know I have used the word “nuances” maybe three times now on this blog, but that is exactly what they are. These subtle differences Python has from other languages. However, Dr. Downing almost always proves how much intent goes behind these methods, and that is the most exciting part - gaining some insight from the creators and developers of the language. 

## What's your pick-of-the-week or tip-of-the-week? 

[The CSS Design Awards website](https://www.cssdesignawards.com/) showcases amazingly designed web pages. I learned of this from a graphic design friend and I think it’s a great origin of inspiration for anyone looking into making a highly developed website. 
