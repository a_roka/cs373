---
title: Week 2
date: 2018-09-09
---

## What did you do this past week? 

This past week started off with one of my busiest Labor Day Mondays ever. I spent the time off solidifying workflow schedules for each of my classes and seeing what free time I can put towards personal projects, research, and studying for interviews. Towards the late afternoon is when our lab actually had the first meeting for the semester, and that was very fun with everyone introducing themselves. Each of us got a feel for what we can learn and contribute to the team and it turns out we’re starting with an almost entirely new generation of undergraduate students. The rest of the week was a usual day-to-day. The readings for this class so far have been pretty insightful and Extreme Programming Installed is definitely a great book to reaffirm what’s expected in the industry. 

## What's in your way? 

Settling into the research lab I joined has actually relieved a lot of stress. Everything I am doing in relation to studies and extracurriculars this entire semester is enjoyable and I’m glad there’s nothing on my plate that I don’t already want to do. That being said, it took a while to find a way to properly distribute my time to each project. What I’ve done the least since classes began again is prepare for interviews, and that should probably be a higher priority for me. 

## What will you do next week? 

Next week I will be sinking my teeth into a compilation of study materials I have for technical interviews and practicing a number of coding challenges on sites just like HackerRank. Finishing Collatz is also on the agenda, but I have two small projects and a lab report that will be due earlier in the week so those are getting polished first. 

## What's your experience of the class? 

This class is very engaging and interactive. This kind of active learning is what I’m actually experiencing in all of my courses this semester which is terrific. Yay for upper-division courses! 

## What's your pick-of-the-week or tip-of-the-week? 

My tip-of-the-week is actually going to be a little bit abstract: go out and socialize. Right before I left from a long work session in the lab on Friday one of the graduate students asked what plans I had for the weekend. I came up blank because I had none besides completing classwork. It can be easy and sometimes not even optional for us to forget about our social lives for extensive periods of time (I’m looking at you, CS439) during our college career even though they should be nearly or just as important as our work lives. This also ties into networking, which is pertinent to many of us searching for internships and full-time offers. Whether it’s in academia or the industry, who you know is quite important and can mean a lot towards where you go! Don't be afraid to make conversation with recruiters and tablers! 