---
title: Week 1
date: 2018-09-01
---

## What did you do this past week? 

Besides preparing for the first days of classes, I used a lot of my free time this week to freshen up my resume and start on a new personal project. I also applied to a few undergraduate research positions - most of which in the field of biochemistry, genetics, and bioinformatics. 
The only assignments so far have been this blog and some reading, along with a physics lab that’s due next Wednesday, so syllabus week has been pretty tame. I’m definitely enjoying all of these upper division classes. The professors are very engaging and make lectures fun and easy to pay attention to. 

## What's in your way? 

I've recently followed through with a lot of my final goals for my college career. Participating in research and preparing for the career fair to land an internship are my main focuses as of right now. Besides that, the apartment I am living in has had a broken air conditioning unit for three days and the property managers say it might not get fixed for another week… It’s currently 103 degrees Fahrenheit inside. 

## What will you do next week?

Next week I will be taking a few tours of research labs that have responded with open undergraduate positions. I’m very excited to be able to discuss with the professors where I can fit in and what work I could do. I'll also be exploring a few new features to add to this blog while I build it almost from scratch. I don’t plan on making this website super fancy or using templates to run it like a bootstrap page, but I am going to try to implement a few more complex items while keeping it clean and simple. If you want to have a chuckle, Google search “every bootstrap website ever” and click on the first result. Undoubtedly, that design has become quite universal so kudos to whoever created it and to whoever popularized it. 

## What are your expectations of the class? 

I expect this class to help me hone my software programming skills and fine-tune my versatility in the languages used by most software engineers. I've heard this class is a great way to experience much of what you would expect to do in the industry, so that is what I want to take advantage of. 

## What's your pick-of-the-week or tip-of-the-week?

My pick-of-the-week is going to be [this website](https://www.hackathon.com/city/us/austin) that lists a bunch of hackathons (more importantly the upcoming ones). There’s even a few that are done online. For some reason, [HackTX](https://hacktx.com/) isn’t on there, but I guess we all already know about that one. 